﻿// See https://aka.ms/new-console-template for more information
using NWaves.Signals;
using NWaves.Signals.Builders;
using NWaves.Transforms;

Console.WriteLine("Staring ...");


DiscreteSignal sinusoid1 = 
    new SineBuilder()
        .SetParameter("frequency", 500.0/*Hz*/)
        //.SetParameter("phase", Math.PI / 6)
        .OfLength(5000)
        .SampledAt(8000/*Hz*/)
        .Build();


DiscreteSignal sinusoid2 = 
    new SineBuilder()
        .SetParameter("frequency", 550.0/*Hz*/)
        //.SetParameter("phase", Math.PI / 6)
        .OfLength(5000)
        .SampledAt(8000/*Hz*/)
        .Build();


var mixed = sinusoid1 + sinusoid2;

// write the samples to a csv file
Console.WriteLine("Writing samples to a csv file ...");
using (var writer = new StreamWriter("mixed.csv"))
{
    writer.WriteLine("n,x");
    for (int i=0; i<mixed.Length; i++)
    {
        writer.WriteLine($"{i},{mixed[i]}");
    }
}


var stft = new Stft(1024, 512);

var freq = stft.AveragePeriodogram(mixed.Samples);

// write the power spectrum to a csv file
Console.WriteLine("Writing power spectrum to a csv file ...");
using (var writer = new StreamWriter("power.csv"))
{
    writer.WriteLine("n,power");
    for (int i=0; i<freq.Length; i++)
    {
        writer.WriteLine($"{i},{freq[i]}");
    }
}