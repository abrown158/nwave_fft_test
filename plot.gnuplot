set datafile separator ','

set multiplot layout 2,1

set xrange[0:1000]
plot "mixed.csv" using 1:2 with lines

set xrange[0:256]
plot "power.csv" using 1:2 with lines

pause -1
